function LuckResult (props){


    return (
        <div>
            <h2> ชื่อ  {props.name}</h2>
            <h3> อายุ {props.age} </h3>
            <h3> ปีเกิด {props.years} </h3>
            <h3> ดวงชะตาของคุณ {props.result} </h3>
        </div>
    );
}

export default LuckResult;