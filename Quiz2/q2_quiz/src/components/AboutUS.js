
function AboutUs (props){


    return (
        <div>
            <h2> จัดทำโดย นางสาว กัลย์สุดา แก่นอินทร์ {props.name}</h2>
            <h2> รหัสนักศึกษา 6310210029 {props.address} </h2>
            <h2> คณะ วิทยาศาสตร์{props.province} </h2>
        </div>
    );
}

export default AboutUs;