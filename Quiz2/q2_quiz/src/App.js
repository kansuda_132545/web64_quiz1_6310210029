import './App.css';
import AboutUs from './component/AboutUS';
import LuckResult from './component/LuckResult';
import Headers from './component/Header';
import LuckPage from './page/LuckPage';

import {Routes, Route,} from "react-router-dom"

function App() {
  return (
    <div className="App">
      <Headers/>
      <Routes>
        <Route path = "about" element={
            <AboutUs/>
          }/>
    
          <Route path ="/" element={
            <LuckResult/>
          }/>
        
          </Routes>
    </div>
  );
}

export default App;
